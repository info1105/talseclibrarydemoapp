//
//  AppDelegate.swift
//  TalsecDemoApp
//
//  Created by Jakub Mejtský on 25/05/2019.
//  Copyright © 2019 AHEAD iTec. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

