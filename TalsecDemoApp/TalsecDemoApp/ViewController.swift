//
//  ViewController.swift
//  TalsecDemoApp
//
//  Created by Jakub Mejtský on 25/05/2019.
//  Copyright © 2019 AHEAD iTec. All rights reserved.
//

import UIKit
import TalsecRuntime

class ViewController: UIViewController {

    @IBOutlet weak var resultsLabel: UILabel!
    private let key = "app.talsec.externalid"
    private let demoText = "⚠️⚠️Demo mode⚠️⚠️ \nDebug mode detection, \nSimulator detection, \nRepackaging validation (bundleID, teamID), \nHook detection. are not called.\n\n"
    
    override func viewDidLoad() {
        if UserDefaults.standard.string(forKey: key) == nil {
            UserDefaults.standard.set(UUID().uuidString, forKey: key)
        }
        SecurityThreatContainer.shared.onChangeCallback = { [weak self] in
            self?.dataChanged()
        }
        dataChanged()
    }

    func dataChanged() {
        DispatchQueue.main.async {
            self.resultsLabel.text = self.demoText + SecurityThreat.allCases.reduce("Results: ", { (result, threat) -> String in
                return result + "\n\(threat.rawValue) - \(SecurityThreatContainer.shared.registeredErrors.contains(threat) ? "NOK" : "OK")"
            })
        }
    }
}
