//
//  SecurityThreatHandler.swift
//  TalsecDemoApp
//
//  Created by Jakub Mejtský on 27/05/2019.
//  Copyright © 2019 AHEAD iTec. All rights reserved.
//

import Foundation
import TalsecRuntime

class SecurityThreatContainer {
    static let shared = SecurityThreatContainer()

    var onChangeCallback: (() -> Void)?
    var registeredErrors: [SecurityThreat] = [] {
        didSet {
            onChangeCallback?()
        }
    }
}


extension SecurityThreatCenter: SecurityThreatHandler {
    
    public func threatDetected(_ securityThreat: SecurityThreat) {
        SecurityThreatContainer.shared.registeredErrors.append(securityThreat)
    }
}
